import torch
from ray import air, tune
from ray.tune import choice, loguniform, with_parameters, run
from ray.tune.integration.pytorch_lightning import TuneReportCallback
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
from torch_geometric.loader import DataLoader
from ticls_gnn.utils import (inspect_results, load_raw_csv_data, 
    load_generated_data)
from ticls_gnn.graph_creation import Grapher, CustomDataTransformer
from ticls_gnn.model import TICLS
from ticls_gnn.config import (
    LABELS, SAMPLE_SIZE, SELF_LOOPS, RANDOM_SEED,
    TRAIN_DIR, TEST_DIR, EVAL_DIR, GENERATED_DATA_PATH, 
    TRAIN_DATA_CONFIG, TEST_DATA_CONFIG, DATA_TRANSFORMS, 
    BATCH_SIZE, EPOCHS, EARLY_STOPPING_PARAMS, MODEL_CHECKPOINT_PARAMS, 
    MODEL_PARAMS
)

torch.manual_seed(12345)

# --- DATA-PREP ---

generated_data = load_generated_data(GENERATED_DATA_PATH)

if generated_data is None:
    grapher = Grapher(
        labels=LABELS,
        sample_size=SAMPLE_SIZE,
        self_loops=SELF_LOOPS
    )

    data_transformer = CustomDataTransformer(transforms=DATA_TRANSFORMS)

    data_dir_paths = {
        "train": TRAIN_DIR, 
        "test": TEST_DIR, 
        "eval": EVAL_DIR
    }

    data_train, data_test, data_eval = load_raw_csv_data(
        data_dir_paths=data_dir_paths,
        train_data_config=TRAIN_DATA_CONFIG,
        test_eval_data_config=TEST_DATA_CONFIG,
        grapher_obj=grapher,
        data_transformer=data_transformer
    )

else:
    data_train, data_test, data_eval, data_summary = generated_data
    MODEL_PARAMS["hparams"]["GENERAL"]["SAMPLE_SIZE"] = data_summary[
        "SAMPLE_SIZE"
    ]
    MODEL_PARAMS["hparams"]["GENERAL"]["SELF_LOOPS"] = data_summary[
        "SELF_LOOPS"
    ]
    MODEL_PARAMS["hparams"]["GENERAL"]["DATA_TRANSFORMS"] = data_summary[
        "DATA_TRANSFORMS"
    ]
    MODEL_PARAMS["hparams"]["TRAIN_DATA_CONFIG"] = data_summary[
        "TRAIN_DATA_CONFIG"
    ]
    MODEL_PARAMS["hparams"]["TEST_DATA_CONFIG"] = data_summary[
        "TEST_DATA_CONFIG"
    ]




# --- MODEL ---

model = TICLS(
    input_channels=data_train[0].x.size(1),
    **MODEL_PARAMS
)


# --- TUNING ---
# tune: batch, lr, droput, lrelu neg slope, use_prev, 

def tune_train_ticls(tuning_config, model_params):
    MODEL_PARAMS = model_params
    MODEL_PARAMS['hparams']['GENERAL']['BATCH_SIZE']=tuning_config['BATCH_SIZE']
    MODEL_PARAMS['hparams']['hidden_channels_gat1']=tuning_config['hidden_channels_gat1']
    MODEL_PARAMS['hparams']['hidden_channels_gat2']=tuning_config['hidden_channels_gat2']
    MODEL_PARAMS['hparams']['learning_rate']=tuning_config['learning_rate']
    # MODEL_PARAMS['hparams']['heads_gat1']=tuning_config['heads_gat1']
    # MODEL_PARAMS['hparams']['heads_gat2']=tuning_config['heads_gat2']
    # MODEL_PARAMS['hparams']['dropout']=tuning_config['dropout']
    # MODEL_PARAMS['hparams']['lrelu_neg_slope']=tuning_config['lrelu_neg_slope']
    # MODEL_PARAMS['hparams']['use_prev_layer_input_on_skip_conn']=tuning_config['use_prev_layer_input_on_skip_conn']

    MODEL_PARAMS['hparams']['MODEL_PARAMS']['hidden_channels_gat1']=tuning_config['hidden_channels_gat1']
    MODEL_PARAMS['hparams']['MODEL_PARAMS']['hidden_channels_gat2']=tuning_config['hidden_channels_gat2']
    MODEL_PARAMS['hparams']['MODEL_PARAMS']['learning_rate']=tuning_config['learning_rate']
    # MODEL_PARAMS['hparams']['MODEL_PARAMS']['heads_gat1']=tuning_config['heads_gat1']
    # MODEL_PARAMS['hparams']['MODEL_PARAMS']['heads_gat2']=tuning_config['heads_gat2']
    # MODEL_PARAMS['hparams']['MODEL_PARAMS']['dropout']=tuning_config['dropout']
    # MODEL_PARAMS['hparams']['MODEL_PARAMS']['lrelu_neg_slope']=tuning_config['lrelu_neg_slope']
    # MODEL_PARAMS['hparams']['MODEL_PARAMS']['use_prev_layer_input_on_skip_conn']=tuning_config['use_prev_layer_input_on_skip_conn']

    BATCH_SIZE=tuning_config['BATCH_SIZE']

    train_loader = DataLoader(
        data_train, 
        batch_size=BATCH_SIZE, 
        shuffle=True
    )

    test_loader = DataLoader(
        data_test+data_eval[1:3], 
        batch_size=BATCH_SIZE, 
        shuffle=False
    )

    model = TICLS(
        input_channels=data_train[0].x.size(1),
        **MODEL_PARAMS
    )

    metrics = {'loss': 'loss/val', 'acc': 'accuracy/val'}
    tune_cb = TuneReportCallback(metrics, on="validation_end")
    early_stopping_cb = EarlyStopping(**EARLY_STOPPING_PARAMS)
    model_checkpoint_cb = ModelCheckpoint(**MODEL_CHECKPOINT_PARAMS)

    callbacks=[
        tune_cb,
        early_stopping_cb,
        model_checkpoint_cb
    ]

    trainer = Trainer(
        max_epochs=EPOCHS,
        val_check_interval=len(train_loader),
        log_every_n_steps=2,
        # check_val_every_n_epoch=2,
        callbacks=callbacks
    )

    trainer.fit(model, train_loader, test_loader)


# TUNER PARAMTERS

GRPUS_PER_TRIAL = 1

TRIALS_DIR = "/scripts/solution/src/model_ht/trials"
TRAINING_DIR = "/scripts/solution/src/model_ht"

# sample ten different parameter combinations
NUM_SAMPLES=5

tuning_config = {
    "BATCH_SIZE":choice([8, 16, 32]),
    "hidden_channels_gat1":choice([256, 512]),
    "hidden_channels_gat2":choice([128, 256, 512]),
    "learning_rate":loguniform(1e-4, 1e-1),
    # "heads_gat1":choice([1,2,3]),
    # # "heads_gat2":choice([1,2,3]),
    # "dropout":choice([0.1, 0.2, 0.5]),
    # "lrelu_neg_slope":choice([0.01, 0.1, 0.2, 0.4]),
    # "use_prev_layer_input_on_skip_conn":choice([True, False])
}

ticls_to_train = with_parameters(
    tune_train_ticls,
    model_params=MODEL_PARAMS
)

analysis = run(
    ticls_to_train,
    reuse_actors=True,
    verbose=1,
    local_dir=TRIALS_DIR,
    resources_per_trial={
        "cpu": 2,
        "gpu": GRPUS_PER_TRIAL
    },
    metric="loss",
    mode="min",
    config=tuning_config,
    num_samples=NUM_SAMPLES,
    name="tune_ticls"
)


best_trial = analysis.best_trial  # Get best trial
best_config = analysis.best_config  # Get best trial's hyperparameters
best_logdir = analysis.best_logdir  # Get best trial's logdir
best_checkpoint = analysis.best_checkpoint  # Get best trial's best checkpoint
best_result = analysis.best_result  # Get best trial's last results

print(f"\n\tBest trial: ", best_trial)
print(f"\tBest config: ", best_config)
print(f"\tBest log dir: ", best_logdir)
print(f"\tBest result: ", best_result)

import torch
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
from torch_geometric.loader import DataLoader
from ticls_gnn.utils import (inspect_results, load_raw_csv_data, 
    load_generated_data)
from ticls_gnn.graph_creation import Grapher, CustomDataTransformer
from ticls_gnn.model import TICLS
from ticls_gnn.config import (
    LABELS, SAMPLE_SIZE, SELF_LOOPS, RANDOM_SEED,
    TRAIN_DIR, TEST_DIR, EVAL_DIR, GENERATED_DATA_PATH, 
    TRAIN_DATA_CONFIG, TEST_DATA_CONFIG, DATA_TRANSFORMS, 
    BATCH_SIZE, EPOCHS, EARLY_STOPPING_PARAMS, MODEL_CHECKPOINT_PARAMS, 
    MODEL_PARAMS
)

torch.manual_seed(12345)

# --- DATA-PREP ---

generated_data = load_generated_data(GENERATED_DATA_PATH)

if generated_data is None:
    grapher = Grapher(
        labels=LABELS,
        sample_size=SAMPLE_SIZE,
        self_loops=SELF_LOOPS
    )

    data_transformer = CustomDataTransformer(transforms=DATA_TRANSFORMS)

    data_dir_paths = {
        "train": TRAIN_DIR, 
        "test": TEST_DIR, 
        "eval": EVAL_DIR
    }

    data_train, data_test, data_eval = load_raw_csv_data(
        data_dir_paths=data_dir_paths,
        train_data_config=TRAIN_DATA_CONFIG,
        test_eval_data_config=TEST_DATA_CONFIG,
        grapher_obj=grapher,
        data_transformer=data_transformer
    )

else:
    data_train, data_test, data_eval, data_summary = generated_data
    SAMPLE_SIZE = data_summary["SAMPLE_SIZE"]
    MODEL_PARAMS["hparams"]["GENERAL"]["SAMPLE_SIZE"] = data_summary[
        "SAMPLE_SIZE"
    ]
    MODEL_PARAMS["hparams"]["GENERAL"]["SELF_LOOPS"] = data_summary[
        "SELF_LOOPS"
    ]
    MODEL_PARAMS["hparams"]["GENERAL"]["DATA_TRANSFORMS"] = data_summary[
        "DATA_TRANSFORMS"
    ]
    MODEL_PARAMS["hparams"]["TRAIN_DATA_CONFIG"] = data_summary[
        "TRAIN_DATA_CONFIG"
    ]
    MODEL_PARAMS["hparams"]["TEST_DATA_CONFIG"] = data_summary[
        "TEST_DATA_CONFIG"
    ]


train_loader = DataLoader(
    data_train, 
    batch_size=BATCH_SIZE, 
    shuffle=True
)

test_loader = DataLoader(
    data_test+data_eval[1:3], 
    batch_size=BATCH_SIZE, 
    shuffle=False
)

for step, data in enumerate(train_loader):
    print(f'Step {step + 1}:')
    print('=======')
    print(f'Number of graphs in the current batch: {data.num_graphs}')
    print(data)
    print()


# --- MODEL ---

model = TICLS(
    input_channels=data_train[0].x.size(1),
    **MODEL_PARAMS
)


# --- TRAINER ---

early_stopping_cb = EarlyStopping(**EARLY_STOPPING_PARAMS)
model_checkpoint_cb = ModelCheckpoint(**MODEL_CHECKPOINT_PARAMS)

trainer = Trainer(
    max_epochs=EPOCHS,
    val_check_interval=len(train_loader),
    log_every_n_steps=2,
    # check_val_every_n_epoch=2,
    callbacks=[
        early_stopping_cb,
        model_checkpoint_cb
    ]
)

trainer.fit(model, train_loader, test_loader)

best_model = TICLS.load_from_checkpoint(
    model_checkpoint_cb.best_model_path,
    input_channels=data_train[0].x.size(1),
    **MODEL_PARAMS
)


# --- SHORT TEST ---
print("\n--- SHORT TEST: ---")
data = data_test[-1]
out = best_model(data.x, data.edge_index)
inspect_results(
    out=out,
    sample_size=SAMPLE_SIZE
)

print("\n\n")

data = data_eval[0]
out = best_model(data.x, data.edge_index)
inspect_results(
    out=out,
    sample_size=SAMPLE_SIZE
)

import pandas as pd
from os.path import join
from torch_geometric.data import Data
from ticls_gnn.model import TICLS
from ticls_gnn.predictor import Predictor
from ticls_gnn.graph_creation import Grapher, CustomDataTransformer
from ticls_gnn.config import (
    LABELS, SAMPLE_SIZE, SELF_LOOPS, DATA_TRANSFORMS
)


# investigate the checkpoint itself
# ckpt = torch.load(join(MODEL_STORE_DIR, 'model.ckpt'))


model = TICLS.load_from_checkpoint(
    './model/model.ckpt'
).eval()

print(f"Model params:\n-------------\n{model.hparams}")

SAMPLE_SIZE = model.hparams["hparams"]['GENERAL']['SAMPLE_SIZE']
SELF_LOOPS = model.hparams["hparams"]['GENERAL']['SELF_LOOPS']
INCLUDE_SUB_LABEL_GRAPH = model.hparams["hparams"]['TEST_DATA_CONFIG']['include_coo_sub_label_edges']


data_transformer = CustomDataTransformer(transforms=DATA_TRANSFORMS)

grapher = Grapher(
    labels=LABELS,
    sample_size=SAMPLE_SIZE,
    self_loops=SELF_LOOPS
)


df_path = "./data/train_test_eval/train/train-1.csv"
df = pd.read_csv(df_path, dtype=str)


predictor = Predictor(
    sample_size=SAMPLE_SIZE,
    labels=LABELS,
    include_sub_label_graph=INCLUDE_SUB_LABEL_GRAPH,
    model=model,
    grapher_obj=grapher,
    data_transformer_obj=data_transformer
)

preds = predictor.predict(df)

print(preds)


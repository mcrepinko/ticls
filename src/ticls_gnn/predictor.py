import numpy as np
import pandas as pd
import torch
from copy import deepcopy
from typing import List, Tuple
from torch_geometric.data import Data
from ticls_gnn.model import TICLS
from ticls_gnn.graph_creation import Grapher, CustomDataTransformer



class Predictor():
    def __init__(
        self,
        sample_size: int,
        labels: List[str],
        include_sub_label_graph: bool,
        model: TICLS,
        grapher_obj: Grapher,
        data_transformer_obj: CustomDataTransformer
    ):
        self.sample_size = sample_size
        self.labels = np.array(labels)
        self.include_sub_label_graph = include_sub_label_graph
        self.model = model
        if self.model.training:
            self.model.eval()
        self.grapher = grapher_obj
        self.data_transformer = data_transformer_obj


    def construct_predictions(
        self,
        sample_size: int,
        column_names: List[str],
        column_labels: List[Tuple[torch.Tensor, torch.Tensor]]
    ):
        predictions = [
            {
                "column_name": column_names[col_idx],
                "predictions": [
                    {
                        self.labels[
                            column_result[0][col_res_idx].item()
                        ]:round(
                            column_result[1][col_res_idx].item()/(sample_size),
                            ndigits=4
                        )
                    }
                    for col_res_idx in range(column_result[0].size(0))
                ]
            }
            for col_idx, column_result in enumerate(column_labels)
        ]

        return predictions


    def process_results(
        self,
        out: torch.Tensor,
        sample_size: int,
        columns: List[str]=None,
        verbose: bool=False
    ):
        sample_size_w_column_name = sample_size + 1
        num_cols = int(out.size(0)/(sample_size_w_column_name))

        if not columns:
            columns = list(range(num_cols))

        start = 0
        column_labels = []
        node_labels = torch.argmax(out, dim=1) # most probable label class for every node

        for col, col_name in enumerate(columns):
            end = (col+1)*(sample_size_w_column_name)

            if verbose:
                # print(f"column #{col}: '{columns[col]}', node indices from..to: {start}..{end-1}")
                print(f"column #{col}: '{col_name}'")
                print(f"node labels: {node_labels[start:end:]}")
                print()

            unq_col_labels = torch.unique(
                node_labels[start:end:],
                return_counts=True
            )
            column_labels.append(unq_col_labels)

            start=deepcopy(end)


        predictions = self.construct_predictions(
            sample_size=sample_size_w_column_name,
            column_names=columns,
            column_labels=column_labels
        )

        return predictions


    def preprocess_data(self, df: pd.DataFrame) -> Data:
        df = df.astype(str)

        node_embeddings, node_labels, graph_edges_coo = self.grapher.construct(
            df=df,
            include_coo_sub_label_edges=self.include_sub_label_graph
        )

        data = self.data_transformer(Data(
            x=node_embeddings, y=node_labels, edge_index=graph_edges_coo
        ))

        return data


    def predict(
        self,
        df: pd.DataFrame,
        verbose: bool=False
    ):
        data = self.preprocess_data(df=df)

        logits = self.model(data.x, data.edge_index)

        return self.process_results(
            out=logits,
            sample_size=self.sample_size,
            columns=df.columns.to_list(),
            verbose=verbose
        )

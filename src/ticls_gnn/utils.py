import torch
import yaml
import pickle
import datetime
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import torch_geometric.transforms as T
from os import walk, path, makedirs
from os.path import join
from copy import deepcopy
from typing import List, Dict, Tuple, Any, Union
from torch_geometric.data import Data
from sklearn.manifold import TSNE
from ticls_gnn.config import RANDOM_SEED
from ticls_gnn.graph_creation import Grapher, CustomDataTransformer


def write_graph_data_report(
    data_summary: Dict[str, Union[str,int,float,bool]],
    file_path: str
) -> int:
    data_hash = hash("".join([
        str(data_summary[key]) for key in list(data_summary.keys())
    ]))
    data_summary["HASH"]=data_hash

    with open(file_path, "w") as out_file:
        yaml.dump(data_summary, out_file)

    return data_hash


def write_graph_data(data: Data, file_path: str) -> None:
    with open(file_path, "wb") as out_file:
        pickle.dump(data, out_file)


def read_graph_data(file_path: str) -> Data:
    try:
        with open(file_path, "rb") as in_file:
            content = pickle.load(in_file)
    except:
        content=None

    return content


def read_graph_data_report(file_path: str) -> Dict[
    str, Union[str,int,float,bool]
]:
    try:
        with open(file_path, "r") as in_file:
            content = yaml.load(in_file, yaml.FullLoader)
    except:
        content = None
    return content


def store_graph_data(
    root_path: str,
    data_summary: Dict[str, Union[str,int,float,bool]],
    train_data: Data,
    test_data: Data=None,
    eval_data: Data=None
) -> int:
    data_version_folder_path = datetime.datetime.now().strftime(
        "%m%d%Y_%H%M%S"
    )

    if "SAMPLE_SIZE" in data_summary.keys():
        data_version_folder_path = "_".join([
            data_version_folder_path, str(data_summary["SAMPLE_SIZE"])
        ])

    root_version_path = join(root_path, data_version_folder_path)

    if not path.exists(root_version_path):
        makedirs(root_version_path)

    data_file_name = "train_data.pkl"
    data_path = join(root_version_path, data_file_name)

    write_graph_data(train_data, data_path)

    if test_data is not None:
        data_file_name = "test_data.pkl"
        data_path = join(root_version_path, data_file_name)
        write_graph_data(test_data, data_path)

    if eval_data is not None:
        data_file_name = "eval_data.pkl"
        data_path = join(root_version_path, data_file_name)
        write_graph_data(eval_data, data_path)

    summary_file_name = "data_summary.yaml"
    summary_file_path = join(
        root_path, data_version_folder_path, summary_file_name
    )
    summary_hash = write_graph_data_report(
        data_summary, summary_file_path
    )

    return summary_hash


def load_generated_data(directory_path: str) -> Union[
    Tuple[Data, Union[Dict[str, Union[str,int,float,bool]], None]],
    Tuple[Data, Data, Union[Dict[str, Union[str,int,float,bool]], None]],
    Tuple[Data, Data, Data, Union[Dict[str, Union[str,int,float,bool]], None]],
    None
]:
    train_data, test_data, eval_data, data_summary = None, None, None, None
    for _, _, dir_files in walk(directory_path):
        for file_name in dir_files:
            file_path = join(directory_path, file_name)

            if file_name == "train_data.pkl":
                train_data = read_graph_data(file_path)

            elif file_name == "test_data.pkl":
                test_data = read_graph_data(file_path)

            elif file_name == "eval_data.pkl":
                eval_data = read_graph_data(file_path)

            elif file_name == "data_summary.yaml":
                data_summary = read_graph_data_report(file_path)

    result = ()

    if train_data is not None:
        result = (train_data,)

    if test_data is not None:
        result += (test_data,)

    if eval_data is not None:
        result += (eval_data,)

    if data_summary is not None:
        result += (data_summary,)

    if result == ():
        result = None

    return result


def visualize(h, color):
    z = TSNE(
        n_components=2,
        random_state=RANDOM_SEED
    ).fit_transform(h.detach().cpu().numpy())

    plt.figure(figsize=(10,10))
    plt.xticks([])
    plt.yticks([])

    plt.scatter(z[:, 0], z[:, 1], s=70, c=color, cmap="Set2")
    plt.show()


def get_probs(logits):
    out_prob = torch.nn.functional.softmax(logits, dim=1)
    out_prob= torch.round(out_prob, decimals=3)
    return out_prob


def inspect_results(
    out: torch.Tensor,
    sample_size: int,
    columns: List[str]=None
):
    num_cols = int(out.size(0)/(sample_size+1))

    if not columns:
        columns = list(range(num_cols))

    start = 0
    column_labels = []
    node_labels = torch.argmax(out, dim=1) # most probable label class for every node

    for col in range(0, num_cols):
        end = (col+1)*(sample_size+1)

        print(f"col: {col}-{columns[col]}, from..to: {start}..{end-1}")
        print(node_labels[start:end:])

        unq_col_labels = torch.unique(node_labels[start:end:])
        column_labels.append(unq_col_labels)

        print()
        start=deepcopy(end)


def load_raw_csv_data(
    data_dir_paths: Dict[str, str],
    train_data_config: Dict[str, bool],
    test_eval_data_config: Dict[str, bool],
    grapher_obj: Grapher,
    data_transformer: CustomDataTransformer,
) -> Tuple[Data, Data, Data]:
    data_train, data_test, data_eval = [], [], []
    for data_dir_path in list(data_dir_paths.keys()):
        print(f"Processing -- {data_dir_path.upper()} -- data")
        for _, _, data_set_files in walk(data_dir_paths[data_dir_path]):
            data_set_files = [
                join(data_dir_paths[data_dir_path], file_name)
                for file_name in data_set_files
            ]

            if data_dir_path == "train":
                constructed_graphs_from_files = [
                    grapher_obj.construct(
                        df=pd.read_csv(file_name, dtype=str),
                        **train_data_config
                    )
                    for file_name in data_set_files
                ]

                data_train.extend([
                    data_transformer(Data(
                        # node_embeddings
                        x=graph[0],
                        # node_labels
                        y=graph[1],
                        # edge index
                        edge_index=graph[2],
                        # sub-label edge index
                        sublabel_edge_index=graph[3]
                    ))
                    for graph in constructed_graphs_from_files
                ])

            else:
                constructed_graphs_from_files = [
                    grapher_obj.construct(
                        df=pd.read_csv(file_name, dtype=str),
                        **test_eval_data_config
                    )
                    for file_name in data_set_files
                ]

                constructed_graphs_from_files = [
                    data_transformer(Data(
                        # node_embeddings
                        x=graph[0],
                        # node_labels
                        y=graph[1],
                        # edge index
                        edge_index=graph[2],
                        # sub-label edge index
                        sublabel_edge_index=graph[3]
                    ))
                    for graph in constructed_graphs_from_files
                ]

                if data_dir_path == "test":
                    # data_test = constructed_graphs_from_files
                    data_test.extend(constructed_graphs_from_files)

                else:
                    # data_eval = constructed_graphs_from_files
                    data_eval.extend(constructed_graphs_from_files)

    return (data_train, data_test, data_eval)


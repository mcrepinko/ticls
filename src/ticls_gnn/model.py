import torch
import torch.nn.functional as F
import pytorch_lightning as pl
from typing import Dict, Any
from torch_geometric.nn import GATv2Conv
from ticls_gnn.config import RANDOM_SEED



class TICLS(pl.LightningModule):
    """
    """
    def __init__(
        self,
        input_channels: int,
        hidden_channels_gat1: int,
        hidden_channels_gat2: int,
        num_node_classes: int,
        heads_gat1: int=1,
        heads_gat2: int=1,
        heads_gat3: int=1,
        dropout: float=0.5,
        lrelu_neg_slope: float=0.01,
        learning_rate: float=0.001,
        add_self_loops: bool=True,
        use_skip_connection: bool=False,
        use_prev_layer_input_on_skip_conn: bool=False,
        hparams: Dict[str, Any]=None
    ):
        super(TICLS, self).__init__()
        torch.manual_seed(RANDOM_SEED)

        self.dropout = dropout
        self.lrelu_neg_slope = lrelu_neg_slope
        self.learning_rate = learning_rate
        self.use_skip_connection = use_skip_connection
        self.use_prev_layer_input_on_skip_conn = use_prev_layer_input_on_skip_conn

        self.criterion_train = torch.nn.CrossEntropyLoss()
        self.criterion_eval = torch.nn.CrossEntropyLoss(reduction='sum')

        gat2_input = heads_gat1*hidden_channels_gat1
        gat3_input = heads_gat2*hidden_channels_gat2

        if hparams:
            self.save_hyperparameters({
                "input_channels": input_channels,
                **hparams["MODEL_PARAMS"],
                "hparams":hparams
            })

        if use_skip_connection:
            gat2_input += input_channels
            if not self.use_prev_layer_input_on_skip_conn:
                gat3_input += input_channels
            else:
                gat3_input += gat2_input + input_channels

        self.gat1 = GATv2Conv(
            in_channels=input_channels,
            out_channels=hidden_channels_gat1,
            heads=heads_gat1,
            add_self_loops=add_self_loops
        )

        self.gat2 = GATv2Conv(
            in_channels=gat2_input,
            out_channels=hidden_channels_gat2,
            heads=heads_gat2,
            add_self_loops=add_self_loops
        )

        self.gat3 = GATv2Conv(
            in_channels=gat3_input,
            out_channels=num_node_classes,
            heads=heads_gat3,
            add_self_loops=add_self_loops
        )


    def forward(self, x, edge_index):
        # xx -> finally, output from g1
        # xxx -> finally, output from g2
        xx = self.gat1(x, edge_index)
        xx = F.leaky_relu(xx, self.lrelu_neg_slope)
        xx = F.dropout(xx, p=self.dropout, training=self.training)

        if self.use_skip_connection:
            xx = torch.cat([xx, x], dim=1)

        xxx = self.gat2(xx, edge_index)
        xxx = F.leaky_relu(xxx, self.lrelu_neg_slope)
        xxx = F.dropout(xxx, p=self.dropout, training=self.training)

        if self.use_skip_connection:
            if not self.use_prev_layer_input_on_skip_conn:
                xxx = torch.cat([xxx, x], dim=1)
            else:
                xxx = torch.cat([xxx, xx, x], dim=1)

        xxx = self.gat3(xxx, edge_index)
        return xxx


    def training_step(self, batch, batch_idx):
        logits_nc = self.forward(batch.x, batch.edge_index)

        loss_nc = self.criterion_train(logits_nc, batch.y)

        pred = logits_nc.argmax(dim=1)
        correct = int((pred == batch.y).sum())
        accuracy = correct/len(batch.y)

        self.log("loss/train", loss_nc)
        self.log("accuracy/train", accuracy)

        return loss_nc


    def validation_step(self, batch, batch_idx):
        logits_nc = self.forward(batch.x, batch.edge_index)
        pred = logits_nc.argmax(dim=1)

        return logits_nc, pred, batch.y, batch.num_graphs


    def validation_epoch_end(self, validation_step_outputs):
        total_loss = 0.0
        num_correct = 0
        total_nodes = 0
        total_graphs = 0

        for output, pred, labels, num_graphs in validation_step_outputs:
            total_loss += self.criterion_eval(output, labels)

            num_correct += (pred == labels).sum()
            total_nodes += pred.size(0)
            total_graphs += num_graphs

        total_loss = total_loss / total_nodes
        val_accuracy = num_correct / total_nodes

        self.log("accuracy/val", val_accuracy)
        self.log("loss/val", total_loss)


    def configure_optimizers(self):
        return torch.optim.Adam(
            self.parameters(),
            lr=self.learning_rate,
        )

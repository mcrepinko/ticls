from distutils.command.config import config
import numpy as np
import pandas as pd
import torch
from copy import deepcopy
from operator import itemgetter
import torch_geometric.transforms as T
from torch_geometric.data import Data
from typing import List, Tuple, Dict, Set
from flair.embeddings import TransformerDocumentEmbeddings, DocumentEmbeddings
from flair.data import Sentence
from ticls_gnn.config import LABELS, SAMPLE_SIZE, SELF_LOOPS, \
    DEFAULT_NODE_EMBEDDER, RANDOM_SEED

# n -> number of nodes
# m -> number of columns

# ex.: n = 5 (column_name + 4 cell values)
#      m = 2


class CustomDataTransformer:
    def __init__(self, transforms: List[T.BaseTransform]):
        self.transforms = transforms


    def __call__(self, data: Data) -> Data:
        for transform in self.transforms:
            data = transform(data)

        return data


    def __repr__(self) -> str:
        return f'{self.__class__.__name__}({self.transforms})'



class Grapher():
    def __init__(
        self,
        labels: List[str],
        sample_size: int,
        node_embedder: DocumentEmbeddings=None,
        self_loops: bool=True
    ) -> None:
        self.labels = labels
        self.sample_size = sample_size
        self.self_loops = self_loops
        self.node_embedder = node_embedder

        if node_embedder is None:
            print(f"[GRAPHER] - Initializing default node embedder: {DEFAULT_NODE_EMBEDDER}")
            self.node_embedder = TransformerDocumentEmbeddings(
                DEFAULT_NODE_EMBEDDER
            )

        self.label_to_int_code_map = {
            label:idx for idx,label in enumerate(self.labels)
        }

        self.int_code_to_label_map = {
            idx:label for idx,label in enumerate(self.labels)
        }


    def edges_to_coo_format(self, edges: List[Tuple[int, int]]) -> np.array:
        edges = sorted(edges,key=itemgetter(1))
        edges = sorted(edges,key=itemgetter(0))
        coo_edges = np.array([
            [x[0] for x in edges],
            [y[1] for y in edges]
        ])
        return coo_edges


    def construct_ordinary_graph_edges(
        self,
        num_cols: int,
        num_vals_p_col: int,
        num_p_col_neighbors: int=2,
        self_loops: bool=True
    ) -> List[Tuple[int, int]]:
        """Creates an ordinary graph in COO format.

        num_cols: int -> Total number of columns.
        num_vals_p_col: int -> Total number of values in column, including
            column name.
        num_p_col_neighbors: int -> Number of intra-column neignouring nodes,
            each node will be connected with. NB: Second to last node will be
            connected only with the first and last node, and the last node will
            be connected only with the first node.
        self_loops: bool -> If True, self-loops will be added for each node.
        """
        m, n = num_cols, num_vals_p_col
        total_nodes = m*n
        edges = []

        for i in range(0, m):
            col_start_idx = i*n
            n_col_start_idx = n+col_start_idx

            for j in range(col_start_idx+1, n_col_start_idx):
                edges.append((col_start_idx, j)) # col name-to-cell

                # ovo bi trebalo biti varijabilno - u smislu da se moze odabrati
                # s koliko ce se kolona u jednom redu neki cvor povezati
                row_neighbor = j+n
                if row_neighbor < total_nodes:
                    edges.append((j, j+n)) # cell-to-cell in row neighbour

                z = deepcopy(j)

                if not self_loops:
                    z+=1

                while (z <= j+num_p_col_neighbors) and (z < n_col_start_idx):
                    edges.append((j, z))
                    z+=1

        return edges


    def construct_sub_label_edges(
        self,
        sub_label_columns: Dict[str, List[int]],
        num_col_vals: int
    ) -> List[Tuple]:
        edges = []

        for key in sub_label_columns.keys():
            col_idxs = sub_label_columns[key]
            if len(col_idxs) < 2:
                continue
            else:
                edges.extend(
                    [
                        (
                            num_col_vals*col_idxs[i]+val_idx,
                            num_col_vals*col_idxs[j]+val_idx
                        )
                        for i in list(range(0, len(col_idxs)))
                        for j in list(range(i+1, len(col_idxs)))
                        for val_idx in list(range(num_col_vals))
                        if col_idxs[j]-col_idxs[i] > 1
                    ]
                )

        return edges


    def process_nodes(
        self,
        df: pd.DataFrame,
        sample_size: int,
    ) -> Tuple[torch.Tensor, List[int], Dict[str, List[int]]]:
        df = df.astype(str)

        sub_label_columns = {}
        node_embeddings = []
        node_labels = []

        for col_idx, column_name in enumerate(df.columns):
            # print(f"{col_idx} - {column_name}")

            # col_vals = df[column_name].values
            # label, sub_label, col_vals = col_vals[0], col_vals[1], \
            #     [column_name]+list(col_vals[2:2+sample_size])

            label, sub_label = df[column_name][0],df[column_name][1]
            col_vals = [column_name]+df[column_name][2:].sample(
                n=sample_size, replace=True, random_state=RANDOM_SEED
            ).values.tolist()

            column_sentences = [
                Sentence(str(col_value)) for col_value in col_vals
            ]
            self.node_embedder.embed(column_sentences)
            node_embeddings.extend(
                [sentence.embedding for sentence in column_sentences]
            )

            node_labels.extend([label]*len(col_vals))

            if sub_label != label:
                if sub_label not in sub_label_columns.keys():
                    sub_label_columns[sub_label] = []
                sub_label_columns[sub_label].append(col_idx)

        node_embeddings = torch.stack(node_embeddings, dim=0)

        return (node_embeddings, node_labels, sub_label_columns)


    def convert_labels_to_int_code(
        self,
        labels: List[str],
        labels_to_int_map: Dict[str, int]
    ) -> List[int]:
        label_codes = [
            labels_to_int_map[label] for label in labels
        ]
        return label_codes


    def convert_labels_int_codes_to_str(
        self,
        label_int_codes: List[int],
        int_codes_to_label_map: Dict[int, str]
    ) -> List[str]:
        labels = [
            int_codes_to_label_map[int_code] for int_code in label_int_codes
        ]

        return labels


    def calculate_total_ordinary_edges(
        self,
        sample_size: int, # num of column values without column name
        num_col: int, # number of columns
        num_p_col_neighbors: int, # number of intra-column neighbors
        self_loop: bool=True
    ) -> int:
        a = ((sample_size-2)*(1+num_p_col_neighbors))+3
        b = a*num_col
        c = b+(sample_size*(num_col-1))

        if self_loop:
            c = c+(num_col*sample_size)

        return c


    def construct(
        self,
        df: pd.DataFrame,
        include_coo_sub_label_edges: bool=True,
        return_edge_tuples: bool=False,
        return_coo_sub_label_edges: bool=False
    ):
        node_embeddings, node_labels, sub_label_columns = self.process_nodes(
            df=df,
            sample_size=self.sample_size,
        )

        node_labels = self.convert_labels_to_int_code(
            labels=node_labels,
            labels_to_int_map=self.label_to_int_code_map
        )

        node_labels = torch.as_tensor(node_labels)

        ordinary_edges = self.construct_ordinary_graph_edges(
            num_cols=len(df.columns),
            num_vals_p_col=self.sample_size+1,
            self_loops=self.self_loops
        )

        if include_coo_sub_label_edges or return_coo_sub_label_edges:
            sub_label_edges = list(set(self.construct_sub_label_edges(
                sub_label_columns=sub_label_columns,
                num_col_vals=self.sample_size+1
            ))-set(ordinary_edges))

        if not include_coo_sub_label_edges:
            self.graph_edges_coo = torch.as_tensor(
                self.edges_to_coo_format(ordinary_edges)
            )

        else:
            self.graph_edges = ordinary_edges+sub_label_edges
            self.graph_edges_coo = torch.as_tensor(
                self.edges_to_coo_format(self.graph_edges)
            )

        result = (node_embeddings, node_labels, self.graph_edges_coo)

        if return_edge_tuples:
            result += (self.graph_edges,)

        if return_coo_sub_label_edges:
            result += (
                torch.as_tensor(self.edges_to_coo_format(sub_label_edges)),
            )

        return result


def test():
    df = pd.read_csv("../data/train_test_eval/train/train-1.csv")

    grapher = Grapher(
        labels=LABELS,
        sample_size=SAMPLE_SIZE,
        self_loops=SELF_LOOPS
    )

    node_embeddings, node_labels, graph_edges_coo = grapher.construct(
        df=df,
        return_edge_tuples=False
    )

    print(f"NODE EMBEDDINGS:\n")
    print(node_embeddings)

    print(f"NODE LABELS:\n")
    print(node_labels)

    print(f"EDGES (COO format):\n")
    print(graph_edges_coo)


# test()

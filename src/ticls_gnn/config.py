import torch_geometric.transforms as T
from os.path import join

LABELS=[
    "other", "name", "date", "address", "ssn", "phone"
]

# ---DATA-PREP---
SAMPLE_SIZE=5
SELF_LOOPS=True
DEFAULT_NODE_EMBEDDER='bert-base-uncased'
RANDOM_SEED=42

ROOT_GRAPH_DATA_STORE="/scripts/solution/src/data/generated/blob"
# USE_GRAPH_DATA_VERSION=""
USE_GRAPH_DATA_VERSION="08272022_212529_10" # does not contain sub-label edges
# USE_GRAPH_DATA_VERSION="08272022_214507_10" # contains sub-label edges

GENERATED_DATA_PATH=join(ROOT_GRAPH_DATA_STORE, USE_GRAPH_DATA_VERSION)

# TRAIN_DIR='./data/train_test_eval/train'
# TEST_DIR='./data/train_test_eval/test'
# EVAL_DIR='./data/train_test_eval/eval'

TRAIN_DIR='./data/generated/train'
TEST_DIR='./data/generated/test'
EVAL_DIR='./data/train_test_eval/eval'

TRAIN_DATA_CONFIG={
    "include_coo_sub_label_edges": True,
    "return_edge_tuples": False,
    "return_coo_sub_label_edges": True
}
TEST_DATA_CONFIG={
    "include_coo_sub_label_edges": False,
    "return_edge_tuples": False,
    "return_coo_sub_label_edges": True
}
# EVAL_DATA_CONFIG = TEST_DATA_CONFIG
DATA_TRANSFORMS=[
    T.ToUndirected()
]

# ---MODEL TRAINING---
BATCH_SIZE=32
EPOCHS=100

#  -- normal training --
# MODEL_STORE_DIR='/scripts/solution/src/model'

# -- hyper-params tuning --
MODEL_STORE_DIR='/scripts/solution/src/model_ht'

EARLY_STOPPING_PARAMS={
    'monitor':'loss/val',
    'patience':10
}
MODEL_CHECKPOINT_PARAMS={
    'monitor':'loss/val',
    'dirpath':MODEL_STORE_DIR,
    'filename':'model'
}
_MODEL_PARAMS={
    "hidden_channels_gat1":512,
    # "hidden_channels_gat2":256,
    "hidden_channels_gat2":128,
    "num_node_classes":len(LABELS),
    "heads_gat1":3,
    "heads_gat2":1,
    "heads_gat3":1,
    "dropout":0.5,
    "lrelu_neg_slope":0.01,
    # "learning_rate":0.001,
    "learning_rate":0.00022331378497946229,
    "add_self_loops":True,
    "use_skip_connection":True,
    "use_prev_layer_input_on_skip_conn":True,
}
_HYPERPARAMETERS={
    "GENERAL": {
        "SAMPLE_SIZE":SAMPLE_SIZE, "SELF_LOOPS":SELF_LOOPS,
        "DATA_TRANSFORMS":str(DATA_TRANSFORMS),
        "BATCH_SIZE":BATCH_SIZE, "EPOCHS":EPOCHS
    },
    "TRAIN_DATA_CONFIG":TRAIN_DATA_CONFIG,
    "TEST_DATA_CONFIG":TEST_DATA_CONFIG,
    "EARLY_STOPPING_PARAMS":EARLY_STOPPING_PARAMS,
    "MODEL_CHECKPOINT_PARAMS":MODEL_CHECKPOINT_PARAMS,
    "MODEL_PARAMS":_MODEL_PARAMS,
}

MODEL_PARAMS={
    **_MODEL_PARAMS,
    "hparams":_HYPERPARAMETERS
}

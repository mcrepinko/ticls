FROM pytorch/pytorch:1.12.0-cuda11.3-cudnn8-devel

COPY ./requirements.txt ./requirements.txt

RUN apt-get update && apt-get install -y git

RUN pip install --no-cache-dir torch-scatter -f https://data.pyg.org/whl/torch-1.12.0+cu113.html torch-sparse -f https://data.pyg.org/whl/torch-1.12.0+cu113.html -r ./requirements.txt

EXPOSE 8888
